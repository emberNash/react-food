import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import { Footer } from './components/Footer';
import { Header } from './components/Header';
import { Home } from './pages/Home';
import { About } from './pages/About';
import { Contact } from './pages/Contact';
import { Category } from './pages/Category';
import { Recipe } from './pages/Recipe';
import { NotFound } from './pages/NotFound';

function App() {
  return (
    <>
      <Router>
        <Header></Header>
        <main className='container content'>
          <Routes>
            <Route path='/' element={<Home />}></Route>
            <Route path='/about' element={<About />}></Route>
            <Route path='/contacts' element={<Contact />}></Route>
            <Route path='/category/:name' element={<Category />}></Route>
            <Route path='/meal/:idMeal' element={<Recipe />}></Route>
            <Route path='*' element={<NotFound />}></Route>
          </Routes>
        </main>
        <Footer></Footer>
      </Router>
    </>
  );
}

export default App;
