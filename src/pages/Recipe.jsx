import { useParams, useNavigate } from 'react-router-dom';
import { getMealById } from '../api';
import { useEffect, useState } from 'react';
import { Preloader } from '../components/Preloader';
const Recipe = () => {
  const { idMeal } = useParams();
  const [recipe, setRecipe] = useState({});
  const navigate = useNavigate();

  useEffect(() => {
    getMealById(idMeal).then((data) => setRecipe(data.meals[0]));
  }, [idMeal]);

  return (
    <>
      <button className='btn goback' onClick={() => navigate(-1)}>
        <i className='material-icons'> keyboard_arrow_left </i>go back
      </button>
      {!recipe.idMeal ? (
        <Preloader></Preloader>
      ) : (
        <div className='recipe'>
          <img src={recipe.strMealThumb} alt={recipe.strMeal} />
          <h1 className='meal-title'>{recipe.strMeal}</h1>
          <h6 className='meal-category'>{recipe.strCategory}</h6>
          <h6 className='meal-area'>{recipe.strArea}</h6>
          <p className='meal-recipe'>{recipe.strInstructions}</p>

          <table className='striped centered'>
            <thead>
              <tr>
                <th>Ingredient</th>
                <th>Measure</th>
              </tr>
            </thead>

            <tbody>
              {Object.keys(recipe).map((key) => {
                if (key.includes('Ingredient') && recipe[key]) {
                  return (
                    <tr key={key}>
                      <td>{recipe[key]}</td>
                      <td>{recipe[`strMeasure${key.slice(13)}`]}</td>
                    </tr>
                  );
                }
                return null;
              })}
            </tbody>
          </table>

          {recipe.strYoutube ? (
            <div className='row'>
              <h5 style={{ margin: ' 2rem 0 1.5rem' }}>Video Recipe</h5>
              <iframe
                src={`https://www.youtube.com/embed/${recipe.strYoutube.slice(
                  -11
                )}`}
                title={idMeal}
              ></iframe>
            </div>
          ) : null}
        </div>
      )}
    </>
  );
};
export { Recipe };
