import { MealItem } from './MealItem';
import { useNavigate } from 'react-router-dom';

const MealsList = ({ meals }) => {
  const navigate = useNavigate();
  return (
    <>
      <button className='btn goback' onClick={() => navigate(-1)}>
        <i className='material-icons'> keyboard_arrow_left </i>go back
      </button>
      <div className='list'>
        {meals.map((el) => (
          <MealItem key={el.idMeal} {...el}></MealItem>
        ))}
      </div>
    </>
  );
};
export { MealsList };
