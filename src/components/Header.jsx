import { Link } from 'react-router-dom';

function Header() {
  return (
    <nav className='light-green darken-1'>
      <div className='nav-wrapper'>
        <Link to='/' className='brand-logo center'>
          React SPA
        </Link>
        <ul id='nav' className='left hide-on-small-only'>
          <li>
            <Link to='/about'>About</Link>
          </li>
          <li>
            <Link to='/contacts'>Contacts</Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export { Header };
